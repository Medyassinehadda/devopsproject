package com.ProjetJEE.GestionAgenceLocation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionAgenceLocationApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionAgenceLocationApplication.class, args);
	}

}
