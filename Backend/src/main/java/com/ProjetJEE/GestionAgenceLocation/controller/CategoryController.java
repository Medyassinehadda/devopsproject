package com.ProjetJEE.GestionAgenceLocation.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ProjetJEE.GestionAgenceLocation.entities.Category;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceCategory;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceVoiture;

@Controller
@RequestMapping("/apicategory")
public class CategoryController {

	@Autowired
	ServiceCategory scategory;
	@Autowired
	ServiceVoiture svoiture;
	

	/**************    GET All  **************/
	@GetMapping("/all")
	public String getAllCategory(Model m) {
		List<Category> liste = scategory.getAllCategory();
		m.addAttribute("Categories",liste);
		m.addAttribute("Voitures",svoiture.getAllVoitures());
		return "listeCategories";
	}
	
	/**************    DELETE   *************/
	@GetMapping("/deletecategory/{id}")
	public String deleteCategory(@PathVariable int id)
	{
		scategory.deleteCategory(id);
		return "redirect:/apicategory/all";
	}
	
	/**************    ADD    **************/
	@GetMapping("/addcategory")
	public String addCategory(Model m)
	{
		m.addAttribute("Voitures",svoiture.getAllVoitures());
		return "addcategory";
	}
	
	/**************    SAVE    **************/
	@PostMapping("/savecategory")
	public String saveCategory(@ModelAttribute Category c, Model m ) throws IOException
	{
		Integer id = c.getId();
		scategory.saveCategory(c);
		
		if(id!=null)//modification
		{
			return "redirect:/apicategory/all";
		}
		else {
			m.addAttribute("message","Categorie enregistrée");
			m.addAttribute("Voitures",svoiture.getAllVoitures());
			
			return "addcategory";
		}
		
	} 
	
	/**************    UPDATE  	 **************/
	@GetMapping("/modifiercategory/{id}")
	public String modifierCategory(Model m, @PathVariable("id") int id)
	{
		m.addAttribute("categorie", scategory.getCategory(id));
		return "addcategory";
	}
	
}
