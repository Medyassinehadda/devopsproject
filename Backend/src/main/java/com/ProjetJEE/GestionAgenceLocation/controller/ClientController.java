package com.ProjetJEE.GestionAgenceLocation.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ProjetJEE.GestionAgenceLocation.entities.Client;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceClient;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceContrat;

@Controller
@RequestMapping("/apiclient")
public class ClientController {
	
	@Autowired
	ServiceClient sclient;
	@Autowired
	ServiceContrat scontrat;
	
	
	/**************    GET All  **************/
	@GetMapping("/all")
	public String getAllClient(Model m) {
		List<Client> liste = sclient.getAllClients();
		m.addAttribute("Clients",liste);
		m.addAttribute("Contrats",scontrat.getAllContrats());
		return "listeClients";
	}
	
	/**************    DELETE   *************/
	@GetMapping("/deleteclient/{id}")
	public String deleteClient(@PathVariable int id)
	{
		sclient.deleteClient(id);
		return "redirect:/apiclient/all";
	}
	
	/**************    ADD    **************/
	@GetMapping("/addclient")
	public String addClient(Model m)
	{
		m.addAttribute("Contrats",scontrat.getAllContrats());
		return "addclient";
	}
	
	/**************    SAVE    **************/
	@PostMapping("/saveclient")
	public String saveClient(@ModelAttribute Client c, Model m ) throws IOException
	{
		Integer id = c.getId();
		sclient.saveClient(c);
		
		if(id!=null)							//modification
		{
			return "redirect:/apiclient/all";
		}
		else {
			m.addAttribute("message","Client enregistrée");
			m.addAttribute("Contrats",scontrat.getAllContrats());
			
			return "addclient";
		}
	} 
	
	/**************    UPDATE  	 **************/
	@GetMapping("/modifierclient/{id}")
	public String modifierClient(Model m, @PathVariable("id") int id)  //@PathVariable pour extraire la valeur de l'URL
	{
		m.addAttribute("client",sclient.getClient(id));
		m.addAttribute("Contrats",scontrat.getAllContrats());
		return "addclient";
	}

	/******************************************************************************************************/
	
	/**************    getClientByCin  **************/
	@GetMapping("/clientbycin")
	public String getClientByCin(@RequestParam int cin,Model m) {
		List<Client> liste = sclient.getClientByCin(cin);
		m.addAttribute("Clients",liste);
		return "listeClients";
	}
	
	/**************    getClientByEmail  **************/
	@GetMapping("/clientbyemail")
	public String getClientByEmail(@RequestParam String email, Model m)
	{
		List<Client> liste = sclient.getClientByEmail(email);
		m.addAttribute("Clients",liste);
		return "listeClients";
	}
	
	/**************    getClientByNom  **************/
	@GetMapping("/clientbynom")
	public String getClientByNom(@RequestParam String nom, Model m)
	{
		List<Client> liste = sclient.getClientByNom(nom);
		m.addAttribute("Clients",liste);
		return "listeClients";
	}
	
	/**************    getClientByPrenom  **************/
	@GetMapping("/clientbyprenom")
	public String getClientByPrenom(@RequestParam String pre, Model m)
	{
		List<Client> liste = sclient.getClientByPrenom(pre);
		m.addAttribute("Clients",liste);
		return "listeClients";
	}
	
	/**************    getClientByAdresse **************/
	@GetMapping("/clientbyadresse")
	public String getClientByAdresse(@RequestParam String adresse, Model m)
	{
		List<Client> liste = sclient.getClientByAdresse(adresse);
		m.addAttribute("Clients",liste);
		return "listeClients";
	}
	
	/**************    getClientByNumPermis  **************/
	@GetMapping("/clientbynumpermis")
	public String getClientByNumPermis(@RequestParam int numper, Model m)
	{
		List<Client> liste = sclient.getClientByNumPermis(numper);
		m.addAttribute("Clients",liste);
		return "listeClients";
	}

	/**************    getClientBytelephone  **************/
	@GetMapping("/clientbytel")
	public String getClientBytelephone(@RequestParam int tel, Model m)
	{
		List<Client> liste = sclient.getClientBytelephone(tel);
		m.addAttribute("Clients",liste);
		return "listeClients";
	}
	
	/******************************************************************************************************/
}
