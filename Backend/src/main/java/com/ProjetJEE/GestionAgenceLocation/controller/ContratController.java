package com.ProjetJEE.GestionAgenceLocation.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ProjetJEE.GestionAgenceLocation.entities.Contrat;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceClient;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceContrat;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceVoiture;

@Controller
@RequestMapping("/apicontrat")
public class ContratController {

	@Autowired
	ServiceContrat scontrat;
	@Autowired
	ServiceClient sclient;
	@Autowired
	ServiceVoiture svoiture;
	
	
	/**************    GET All  **************/
	@GetMapping("/all")
	public String getAllContrat(Model m) {
		List<Contrat> liste = scontrat.getAllContrats();
		m.addAttribute("Contrats",liste);
		m.addAttribute("Clients",sclient.getAllClients());
		m.addAttribute("Voitures",svoiture.getAllVoitures());
		return "listeContrats";
	}
	
	/**************    DELETE   *************/
	@GetMapping("/deletecontrat/{id}")
	public String deleteContrat(@PathVariable int id)
	{
		scontrat.deleteContrat(id);
		return "redirect:/apicontrat/all";
	}
	
	/**************    ADD    **************/
	@GetMapping("/addcontrat")
	public String addContrat(Model m)
	{
		m.addAttribute("Clients",sclient.getAllClients());
		m.addAttribute("Voitures",svoiture.getAllVoitures());
		return "addcontrat";
	}
	
	/**************    SAVE    **************/
	@PostMapping("/savecontrat")
	public String saveContrat(@ModelAttribute Contrat c, Model m ) throws IOException
	{
		Integer id = c.getId();
		scontrat.saveContrat(c);
		
		if(id!=null)							//modification
		{
			return "redirect:/apicontrat/all";
		}
		else {
			m.addAttribute("message","Contrat enregistrée");
			m.addAttribute("Clients",sclient.getAllClients());
			m.addAttribute("Voitures",svoiture.getAllVoitures());
			
			return "addcontrat";
		}
	} 
	
	/**************    UPDATE  	 **************/
	@GetMapping("/modifiercontrat/{id}")
	public String modifierContrat(Model m, @PathVariable("id") int id)  //@PathVariable pour extraire la valeur de l'URL
	{
		m.addAttribute("Clients",sclient.getAllClients());
		m.addAttribute("Voitures",svoiture.getAllVoitures());
		m.addAttribute("contrat", scontrat.getContrat(id));
		return "addcontrat";
	}

	/******************************************************************************************************/
	
	/**************    GET CONTRAT BY CLIENT  **************/
	@GetMapping("/contratbyclient")
	public String getContratByClientId(@RequestParam int idc,Model m) {
		List<Contrat> liste = scontrat.getContratByClientId(idc);
		m.addAttribute("Contrats",liste);
		return "listeContrats";
	}
	
	/**************    GET CONTRAT BY VoitureId  **************/
	@GetMapping("/contratbyvoiture")
	public String getContratByVoitureId(@RequestParam int idv, Model m)
	{
		List<Contrat> liste = scontrat.getContratByVoitureId(idv);
		m.addAttribute("Contrats",liste);
		return "listeContrats";
	}
	
	/**************    GET CONTRAT BY Cautionnement  **************/
	@GetMapping("/contratbycautionnement")
	public String getContratByCautionnement(@RequestParam String cau, Model m)
	{
		List<Contrat> liste = scontrat.getContratByCautionnement(cau);
		m.addAttribute("Contrats",liste);
		return "listeContrats";
	}
	
	/**************    GET CONTRAT BY DateDebut  **************/
	@GetMapping("/contratbydatedebut")
	public String getContratByDateDebut(@RequestParam String datedebut, Model m)
	{
		List<Contrat> liste = scontrat.getContratByDateDebut(datedebut);
		m.addAttribute("Contrats",liste);
		return "listeContrats";
	}
	
	/**************    GET CONTRAT BY DateFin  **************/
	@GetMapping("/contratbydatefin")
	public String getContratByDateFin(@RequestParam String datefin, Model m)
	{
		List<Contrat> liste = scontrat.getContratByDateFin(datefin);
		m.addAttribute("Contrats",liste);
		return "listeContrats";
	}

	/**************    GET CONTRAT BY Montant  **************/
	@GetMapping("/contratbymontant")
	public String getContratByMontant(@RequestParam String montant, Model m)
	{
		List<Contrat> liste = scontrat.getContratByMontant(montant);
		m.addAttribute("Contrats",liste);
		return "listeContrats";
	}
	
	/**************    GET CONTRAT BY TypePayement  **************/
	@GetMapping("/contratbytypepayement")
	public String getContratByTypePayement(@RequestParam String p, Model m)
	{
		List<Contrat> liste = scontrat.getContratByTypePayement(p);
		m.addAttribute("Contrats",liste);
		return "listeContrats";
	}

	/**************    GET CONTRAT BY periodeLocation  **************/
	@GetMapping("/contratbyperiodeLocation")
	public String getContratByPeriodeLocation(@RequestParam String pl, Model m)
	{
		List<Contrat> liste = scontrat.getContratByPeriodeLocation(pl);
		m.addAttribute("Contrats",liste);
		return "listeContrats";
	}
	
	/******************************************************************************************************/

}
