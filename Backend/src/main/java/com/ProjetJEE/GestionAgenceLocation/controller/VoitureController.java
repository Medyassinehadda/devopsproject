package com.ProjetJEE.GestionAgenceLocation.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.ProjetJEE.GestionAgenceLocation.dao.CategoryRepository;
import com.ProjetJEE.GestionAgenceLocation.entities.Voiture;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceCategory;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceContrat;
import com.ProjetJEE.GestionAgenceLocation.service.ServiceVoiture;

@Controller
@RequestMapping("/apivoiture")
public class VoitureController {

	@Autowired
	ServiceVoiture svoiture;
	@Autowired
	ServiceCategory scategory;
	@Autowired
	ServiceContrat scontrat;
	@Autowired
	CategoryRepository crep;
	
	
	/**************    GET All  **************/
	@GetMapping("/all")
	public String getAllVoitures(Model m) {
		List<Voiture> liste = svoiture.getAllVoitures();
		m.addAttribute("Voitures",liste);
		m.addAttribute("categories",crep.findAll());
		m.addAttribute("categorie", "Toutes les categories");
		return "listeVoitures";
	}
	
	/**************    DELETE   *************/
	@GetMapping("/deletevoiture/{id}")
	public String deleteVoiture(@PathVariable int id)
	{
		svoiture.deleteVoiture(id);
		return "redirect:/apivoiture/all";
	}
	
	/**************    ADD    **************/
	@GetMapping("/addvoiture")
	public String addVoiture(Model m)
	{
		m.addAttribute("categories",scategory.getAllCategory());
		return "addvoiture";
	}
	
	/**************    SAVE    **************/
	@PostMapping("/savevoiture")
	public String saveVoiture(@ModelAttribute Voiture v,@RequestParam("file") MultipartFile mf, Model m ) throws IOException
	{
		Integer id = v.getId();
		svoiture.saveVoitureWithPicture(v, mf);
		
		if(id!=null)//modification
		{
			return "redirect:/apivoiture/all";
		}
		else {
			m.addAttribute("categories",scategory.getAllCategory());
			m.addAttribute("message","Voiture enregistrée");
			return "addvoiture";
		}
		
	} 
	
	/**************    UPDATE  	 **************/
	@GetMapping("/modifiervoiture/{id}")
	public String modifierVoiture(Model m, @PathVariable("id") int id)
	{
		m.addAttribute("categories",scategory.getAllCategory());
		m.addAttribute("voiture", svoiture.getVoiture(id));
		return "addvoiture";
	}

	/******************************************************************************************************/
	
	/**************    GET VOITURE BY CATEGORY  **************/
	@GetMapping("/voiturebycategory")
	public String getVoitureByCategory(@RequestParam("categorie") int idc,Model m) {
		m.addAttribute("categories", scategory.getAllCategory());
		if (idc == 0) {
			m.addAttribute("liste", svoiture.getAllVoitures());
			return "redirect:/apivoiture/all";
		}
		else {
			m.addAttribute("Voitures", svoiture.getVoitureByCategoryId(idc));
			m.addAttribute("categorie", scategory.getCategory(idc).getGamme());
			return "listeVoitures";
		}
	}
	
	/**************    FIND BY CATEGORY  **************/
//	@GetMapping("/voiturebycategory")
//	public String getVoitureByCategory(@RequestParam String mc, Model m)
//	{
//		List<Voiture> liste = sv.getVoitureByCategoryId(mc);
//		m.addAttribute("Voitures",liste);
//		return "listeVoitures";
//	}
	
	/**************    FIND BY COLOR  **************/
	@GetMapping("/voiturebycouleur")
	public String getVoitureByCouleur(@RequestParam String mc, Model m)
	{
		List<Voiture> liste = svoiture.getVoitureByCouleur(mc);
		m.addAttribute("Voitures",liste);
		return "listeVoitures";
	}
	
	/**************    FIND BY COUT PAR JOUR  **************/
	@GetMapping("/voiturebycoutparjour")
	public String getVoitureByCoutParJour(@RequestParam String mc, Model m)
	{
		List<Voiture> liste = svoiture.getVoitureByCoutParJour(mc);
		m.addAttribute("Voitures",liste);
		return "listeVoitures";
	}
	
	/**************    FIND BY MARQUE  **************/
	@GetMapping("/voiturebymarque")
	public String getVoitureByMarque(@RequestParam String mc, Model m)
	{
		List<Voiture> liste = svoiture.getVoitureByMarque(mc);
		m.addAttribute("Voitures",liste);
		return "listeVoitures";
	}

	/**************    FIND BY MODELE  **************/
	@GetMapping("/voiturebymodele")
	public String getVoitureByModele(@RequestParam String mc, Model m)
	{
		List<Voiture> liste = svoiture.getVoitureByModele(mc);
		m.addAttribute("Voitures",liste);
		return "listeVoitures";
	}
	
	/**************    FIND BY MATRICULE  **************/
	@GetMapping("/voiturebymatricule")
	public String getVoitureByMatricule(@RequestParam String mc, Model m)
	{
		List<Voiture> liste = svoiture.getVoitureByMatricule(mc);
		m.addAttribute("Voitures",liste);
		return "listeVoitures";
	}

	/**************    FIND BY PUISSANCE  **************/
	@GetMapping("/voiturebypuissance")
	public String getVoitureByPuissance(@RequestParam String mc, Model m)
	{
		List<Voiture> liste = svoiture.getVoitureByPuissance(mc);
		m.addAttribute("Voitures",liste);
		return "listeVoitures";
	}
	
	/******************************************************************************************************/
	
}
