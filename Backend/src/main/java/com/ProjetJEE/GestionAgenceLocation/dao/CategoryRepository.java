package com.ProjetJEE.GestionAgenceLocation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ProjetJEE.GestionAgenceLocation.entities.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer>{
	
	@Query("select c from Category c where c.gamme like %:x%")
	public List<Category> rechercheParGamme(@Param("x")String mc);

}
