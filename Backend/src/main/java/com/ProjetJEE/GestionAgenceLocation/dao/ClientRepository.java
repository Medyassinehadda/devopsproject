package com.ProjetJEE.GestionAgenceLocation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ProjetJEE.GestionAgenceLocation.entities.Client;

public interface ClientRepository extends JpaRepository<Client, Integer>{
	
	@Query("select c from Client c where c.cin like %:x%")
	public List<Client> rechercheParCin(@Param("x")int cin);
	
	@Query("select c from Client c where c.email like %:x%")
	public List<Client> rechercheParEmail(@Param("x")String email);
	
	@Query("select c from Client c where c.nom like %:x%")
	public List<Client> rechercheParNom(@Param("x")String nom);
	
	@Query("select c from Client c where c.prenom like %:x%")
	public List<Client> rechercheParPrenom(@Param("x")String prenom);

	@Query("select c from Client c where c.adresse like %:x%")
	public List<Client> rechercheParAdresse(@Param("x")String adresse);
	
	@Query("select c from Client c where c.num_permis like %:x%")
	public List<Client> rechercheParNumPermis(@Param("x")int numpermis);
	
	@Query("select c from Client c where c.telephone like %:x%")
	public List<Client> rechercheParTelephone(@Param("x")int telephone);
	
}
