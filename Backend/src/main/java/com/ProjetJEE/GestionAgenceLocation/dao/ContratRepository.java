package com.ProjetJEE.GestionAgenceLocation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ProjetJEE.GestionAgenceLocation.entities.Contrat;

public interface ContratRepository extends JpaRepository<Contrat, Integer>{
	
	@Query("select c from Contrat c where c.client like %:x%")
	public List<Contrat> rechercheParClientId(@Param("x")int clientid);
	
	@Query("select c from Contrat c where c.voiture like %:x%")
	public List<Contrat> rechercheParVoitureId(@Param("x")int voitureid);
	
	@Query("select c from Contrat c where c.cautionnement like %:x%")
	public List<Contrat> rechercheParCautionnement(@Param("x")String cautionnement);
	
	@Query("select c from Contrat c where c.date_debut like %:x%")
	public List<Contrat> rechercheParDateDebut(@Param("x")String datedebut);
	
	@Query("select c from Contrat c where c.date_fin like %:x%")
	public List<Contrat> rechercheParDateFin(@Param("x")String datefin);
	
	@Query("select c from Contrat c where c.montant like %:x%")
	public List<Contrat> rechercheParMontant(@Param("x")String montant);
	
	@Query("select c from Contrat c where c.type_payement like %:x%")
	public List<Contrat> rechercheParTypePayement(@Param("x")String typepayement);
	
	@Query("select c from Contrat c where c.periode_location like %:x%")
	public List<Contrat> rechercheParPeriodeLocation(@Param("x")String periodelocation);
	
}
