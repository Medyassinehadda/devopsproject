package com.ProjetJEE.GestionAgenceLocation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ProjetJEE.GestionAgenceLocation.entities.Voiture;

public interface VoitureRepository extends JpaRepository<Voiture, Integer>{
	
	@Query("select v from Voiture v where v.matricule like %:x%")
	public List<Voiture> rechercheParMatricule(@Param("x")String matricule);
	
	@Query("select v from Voiture v where v.couleur like %:x%")
	public List<Voiture> rechercheParCouleur(@Param("x")String couleur);
	
	@Query("select v from Voiture v where v.marque like %:x%")
	public List<Voiture> rechercheParMarque(@Param("x")String marque);
	
	@Query("select v from Voiture v where v.modele like %:x%")
	public List<Voiture> rechercheParModele(@Param("x")String modele);
	
	@Query("select v from Voiture v where v.puissance like %:x%")
	public List<Voiture> rechercheParPuissance(@Param("x")String puissance);
	
	@Query("select v from Voiture v where v.cout_par_jour like %:x%")
	public List<Voiture> rechercheParCoutParJour(@Param("x")String coutparjour);
	
	@Query("select v from Voiture v where v.category.id like %:x%")
	public List<Voiture> rechercheParCategoryId(@Param("x")int catgeoryid);
	
//	@Query("select v from Voiture v where v.contrat	 like %:x%")
//	public List<Voiture> rechercheParContratId(@Param("x")int contratid);
	
}
