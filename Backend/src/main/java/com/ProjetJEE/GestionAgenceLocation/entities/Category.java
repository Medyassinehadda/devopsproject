package com.ProjetJEE.GestionAgenceLocation.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data															//permet de remplacer tous les getters et setters des attributs
@NoArgsConstructor												//permet de remplacer le constructeur par défaut
@AllArgsConstructor												//permet de remplacer le constructeur avec paramètres
@Table(name = "CATEGORIES")
public class Category {
	@Id 														//id clé primaire
	@GeneratedValue(strategy = GenerationType.IDENTITY)			//id auto-increment
	private Integer id;
	private String gamme;										//bas - milieu - haut (gamme)
	@OneToMany (mappedBy = "category" , cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Voiture> liste;		//chaque categorie à une liste de voiture
}