package com.ProjetJEE.GestionAgenceLocation.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CLIENTS")
public class Client {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String email;
	private String password;
	private Integer cin;
	private String nom;
	private String prenom;
	private String date_naissance;
	private String adresse;
	private Integer telephone;
	private Integer num_permis;
	@OneToMany(mappedBy = "client")
	private List<Contrat> liste;
}
