package com.ProjetJEE.GestionAgenceLocation.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CONTRATS")
public class Contrat {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String periode_location;
	private String date_debut;
	private String date_fin;
	private String type_payement;
	private String montant;
	private String cautionnement;
	@ManyToOne
	private Client client;
	@OneToOne
	private Voiture voiture;
}
