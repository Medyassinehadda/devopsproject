package com.ProjetJEE.GestionAgenceLocation.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity 						//permet de remplacer tous les getters et setters des attributs
@Data
@NoArgsConstructor				//permet de remplacer le constructeur par défaut
@AllArgsConstructor				//permet de remplacer le constructeur avec paramètres
@Table(name = "VOITURES")
public class Voiture {
	
	@Id														//id clé primaire
	@GeneratedValue(strategy = GenerationType.IDENTITY)		//id auto-increment
	private Integer id;
	private String photo;
	private String matricule;
	private String marque;
	private String modele;
	private String couleur;
	private String puissance;
	@ManyToOne
	private Category category;
	private String cout_par_jour;
	@OneToMany (mappedBy = "voiture" , cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Contrat> liste;
//	@OneToOne
//	@JoinColumn(name = "contrat_id")
//	private Contrat contrat;
}

