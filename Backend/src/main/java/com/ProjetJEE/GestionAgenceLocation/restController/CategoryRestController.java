package com.ProjetJEE.GestionAgenceLocation.restController;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ProjetJEE.GestionAgenceLocation.entities.Category;
import com.ProjetJEE.GestionAgenceLocation.service.IserviceCategory;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin("*")
@RestController
@RequestMapping("/restapicategory")
public class CategoryRestController {

	@Autowired
	IserviceCategory scategory;
	
	/**************    Get All  **************/
	@GetMapping("/all")
	public List<Category> getallCategory() {
		return scategory.getAllCategory();
	}
	
	/***************     ADD    ***************/
	@PostMapping("/save")
	public void saveCategory(@RequestParam("category") String c) throws IOException
	{
		Category cc = new ObjectMapper().readValue(c, Category.class);
		scategory.saveCategory(cc);
	}
	
	/*************** Delete Category ***************/
	@DeleteMapping("/delete/{id}")
	public void deleteCategory(@PathVariable int id) {
		scategory.deleteCategory(id);
	}
	
	/*************** Update Client ***************/
	@GetMapping("/update/{id}")
	public void updateCategory(@PathVariable("id") int id) {
		scategory.getCategory(id);
	}
	
}
