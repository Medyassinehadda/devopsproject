package com.ProjetJEE.GestionAgenceLocation.restController;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ProjetJEE.GestionAgenceLocation.entities.Client;
import com.ProjetJEE.GestionAgenceLocation.service.IserviceClient;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin("*")
@RestController
@RequestMapping("/restapiclient")
public class ClientRestController {

	@Autowired							//permet d'activer l'injection automatique de dépendance
	IserviceClient sclient;
	
	/**************    Get All  **************/
	@GetMapping("/all")
	public List<Client> getAllClient() {
		return sclient.getAllClients();
	}
	
	/***************     ADD    ***************/
	@PostMapping("/saveclient")
	public void saveClient(@RequestParam("client") String c) throws IOException
	{
		Client cc = new ObjectMapper().readValue(c, Client.class);
		sclient.saveClient(cc);
	}
	
	/*************** Delete Client ***************/
	@DeleteMapping("/delete/{id}")
	public void deleteClient(@PathVariable int id) {
		sclient.deleteClient(id);
	}
	
	/*************** Update Client ***************/
	@GetMapping("/modifierclient/{id}")
	public void modifierClient(@PathVariable("id") int id) {
		sclient.getClient(id);
	}

	/**************     getClientByCin  **************/
	@GetMapping("/clientbycin")
	public List<Client> getClientByCin(@RequestParam int cin)
	{
		return sclient.getClientByCin(cin);
	}

	/**************     getClientByEmail  **************/
	@GetMapping("/clientbyemail")
	public List<Client> getClientByEmail(@RequestParam String email)
	{
		return sclient.getClientByEmail(email);
	}

	/**************     getClientByNom  **************/
	@GetMapping("/clientbynom")
	public List<Client> getClientByNom(@RequestParam String nom)
	{
		return sclient.getClientByNom(nom);
	}

	/**************     getClientByPrenom  **************/
	@GetMapping("/clientbyprenom")
	public List<Client> getClientByPrenom(@RequestParam String pre)
	{
		return sclient.getClientByPrenom(pre);
	}
	
	/**************     getClientByAdresse  **************/
	@GetMapping("/clientbyadresse")
	public List<Client> getClientByAdresse(@RequestParam String adr)
	{
		return sclient.getClientByAdresse(adr);
	}
	
	/**************     getClientByNumPermis  **************/
	@GetMapping("/clientbynumpermis")
	public List<Client> getClientByNumPermis(@RequestParam int numper)
	{
		return sclient.getClientByNumPermis(numper);
	}

	/**************     getClientBytelephone  **************/
	@GetMapping("/clientbytel")
	public List<Client> getClientBytelephone(@RequestParam int tel)
	{
		return sclient.getClientBytelephone(tel);
	}
	
}
