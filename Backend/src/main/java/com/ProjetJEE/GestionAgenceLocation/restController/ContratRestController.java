package com.ProjetJEE.GestionAgenceLocation.restController;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ProjetJEE.GestionAgenceLocation.entities.Contrat;
import com.ProjetJEE.GestionAgenceLocation.service.IserviceContrat;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin("*")
@RestController
@RequestMapping("/restapicontrat")
public class ContratRestController {

	@Autowired 
	IserviceContrat scontrat;
	
	/**************    Get All  **************/
	@GetMapping("/all")
	public List<Contrat> getAllContrats() {
		return scontrat.getAllContrats();
	}

	/***************     ADD    ***************/
	@PostMapping("/savecontrat")
	public void saveContrat(@RequestParam("contrat") String c) throws IOException
	{
		Contrat cc = new ObjectMapper().readValue(c, Contrat.class);
		scontrat.saveContrat(cc);
	}

	/*************** Delete Contrat ***************/
	@DeleteMapping("/delete/{id}")
	public void deleteContrat(@PathVariable int id) {
		scontrat.deleteContrat(id);
	}
	
	/*************** Update Contrat ***************/
	@GetMapping("/modifiercontrat/{id}")
	public void modifierContrat(@PathVariable("id") int id)
	{
		scontrat.getContrat(id);
	}
	
	/**************     GET CONTRAT BY CLIENT  **************/
	@GetMapping("/contratbyclient/{id}")
	public List<Contrat> getVoitureByCategory(@PathVariable("id") int id)
	{
		return scontrat.getContratByClientId(id);
	}

	/**************     GET CONTRAT BY VoitureId  **************/
	@GetMapping("/contratbyvoiture/{id}")
	public List<Contrat> getContratByVoitureId(@PathVariable("id") int id)
	{
		return scontrat.getContratByVoitureId(id);
	}
	
	/**************     GET CONTRAT BY Cautionnement  **************/
	@GetMapping("/contratbycautionnement")
	public List<Contrat> getContratByCautionnement(@RequestParam String cau)
	{
		return scontrat.getContratByCautionnement(cau);
	}
	
	/**************     GET CONTRAT BY DateDebut  **************/
	@GetMapping("/contratbydatedebut")
	public List<Contrat> getContratByDateDebut(@RequestParam String datedebut)
	{
		return scontrat.getContratByDateDebut(datedebut);
	}
	
	/**************     GET CONTRAT BY DateFin  **************/
	@GetMapping("/contratbydatefin")
	public List<Contrat> getContratByDateFin(@RequestParam String datefin)
	{
		return scontrat.getContratByDateFin(datefin);
	}
	
	/**************     GET CONTRAT BY Montant  **************/
	@GetMapping("/contratbymontant")
	public List<Contrat> getContratByMontant(@RequestParam String montant)
	{
		return scontrat.getContratByMontant(montant);
	}
	
	/**************     GET CONTRAT BY TypePayement  **************/
	@GetMapping("/contratbytypepayement")
	public List<Contrat> getContratByTypePayement(@RequestParam String p)
	{
		return scontrat.getContratByTypePayement(p);
	}
	
	/**************     GET CONTRAT BY periodeLocation  **************/
	@GetMapping("/getcontratbyperiodelocation")
	public List<Contrat> getContratByPeriodeLocation(@RequestParam String pl)
	{
		return scontrat.getContratByPeriodeLocation(pl);
	}
	
}
