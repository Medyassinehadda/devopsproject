package com.ProjetJEE.GestionAgenceLocation.restController;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ProjetJEE.GestionAgenceLocation.entities.Voiture;
import com.ProjetJEE.GestionAgenceLocation.service.IserviceVoiture;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin("*")
@RestController
@RequestMapping("/restapivoiture")
public class VoitureRestController {

	@Autowired 
	IserviceVoiture svoiture;
	
	/**************    Get All  **************/
	@GetMapping("/all")
	public List<Voiture> getAllVoitures() {
		return svoiture.getAllVoitures();
	}
	
	/**************     GET VOITURE BY CATEGORY  **************/
	@GetMapping("/voiturebycategory/{id}")
	public List<Voiture> getVoitureByCategory(@PathVariable("id") int id)
	{
		return svoiture.getVoitureByCategoryId(id);
	}
	
	/**************    get Image *************/
	@GetMapping(path = "/getImage/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getImage(@PathVariable ("id") int id) throws IOException
	{
		return svoiture.getImage(id);
	}
	
	/***************     ADD    ***************/
	@PostMapping("/savevoiture")
	public void ajouterVoiture(@RequestParam("voiture") String v, @RequestParam("file") MultipartFile mf) throws IOException
	{
		Voiture vv = new ObjectMapper().readValue(v, Voiture.class);
		svoiture.saveVoitureWithPicture(vv, mf);
	}
	
	
	/*************** Delete Voiture ***************/
	@DeleteMapping("/delete/{id}")
	public void deleteVoiture(@PathVariable int id) {
		svoiture.deleteVoiture(id);
	}
	
	
	/*************** Update Voiture ***************/
	@GetMapping("/update/{id}")
	public void mettreAJourVoiture(@PathVariable("id") int id){
		svoiture.getVoiture(id);
	}
	
//	private void saveVoiture(Voiture v, MultipartFile mf) throws IOException {
//		String photo;
//		if(!mf.getOriginalFilename().equals("")) {
//			photo = svoiture.saveImage(mf);
//			v.setPhoto(photo);
//		}
//		svoiture.saveVoiture(v);
//	}

}
