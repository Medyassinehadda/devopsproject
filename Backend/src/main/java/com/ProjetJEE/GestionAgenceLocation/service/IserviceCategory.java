package com.ProjetJEE.GestionAgenceLocation.service;

import java.util.List;

import com.ProjetJEE.GestionAgenceLocation.entities.Category;

public interface IserviceCategory {

	public void saveCategory(Category a);
	public Category getCategory(int id);
	public void deleteCategory(int id);
	public List<Category> getAllCategory();
}
