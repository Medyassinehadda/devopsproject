package com.ProjetJEE.GestionAgenceLocation.service;

import java.util.List;

import com.ProjetJEE.GestionAgenceLocation.entities.Client;

public interface IserviceClient {
	
	public void saveClient(Client a);
	public Client getClient(int id);
	public void deleteClient(int id);
	
	public List<Client> getClientByCin(int cin);
	public List<Client> getClientByEmail(String email);
	public List<Client> getClientByNom(String nom);
	public List<Client> getClientByPrenom(String prenom);
	public List<Client> getClientByAdresse(String adresse);
	public List<Client> getClientByNumPermis(int numpermis);
	public List<Client> getClientBytelephone(int telephone);

	public List<Client> getAllClients();
	
}
