package com.ProjetJEE.GestionAgenceLocation.service;

import java.util.List;

import com.ProjetJEE.GestionAgenceLocation.entities.Contrat;


public interface IserviceContrat {

	public void saveContrat(Contrat c);
	public Contrat getContrat(int id);
	public void deleteContrat(int id);
	
	public List<Contrat> getContratByClientId(int clientid);
	public List<Contrat> getContratByVoitureId(int voitureid);
	public List<Contrat> getContratByCautionnement(String cautionnement);
	public List<Contrat> getContratByDateDebut(String datedebut);
	public List<Contrat> getContratByDateFin(String datefin);
	public List<Contrat> getContratByMontant(String montant);
	public List<Contrat> getContratByTypePayement(String typepayement);
	public List<Contrat> getContratByPeriodeLocation(String periodelocation);

	public List<Contrat> getAllContrats();
	
}
