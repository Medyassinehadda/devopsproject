package com.ProjetJEE.GestionAgenceLocation.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ProjetJEE.GestionAgenceLocation.entities.Voiture;


public interface IserviceVoiture {

	public void saveVoiture(Voiture a);
	public Voiture getVoiture(int id);
	public void deleteVoiture(int id);
	
	public List<Voiture> getVoitureByMatricule(String matricule);
	public List<Voiture> getVoitureByCouleur(String couleur);
	public List<Voiture> getVoitureByMarque(String marque);
	public List<Voiture> getVoitureByModele(String modele);
	public List<Voiture> getVoitureByPuissance(String puissance);
	public List<Voiture> getVoitureByCoutParJour(String coutparjour);
	public List<Voiture> getVoitureByCategoryId(int catgeoryid);
//	public List<Voiture> getVoitureByContratId(int contratid);
	
	//image
	public String saveImage(MultipartFile mf) throws IOException;
	public void saveVoitureWithPicture(Voiture v, MultipartFile mf) throws IOException;
	public void deleteImage(Integer voitureId) throws IOException;
	public void deleteVoiture(Integer id) throws IOException;
	public byte[] getImage(int id) throws IOException;
	
	public List<Voiture> getAllVoitures();
	
}
