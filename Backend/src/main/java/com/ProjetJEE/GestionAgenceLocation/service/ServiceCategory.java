package com.ProjetJEE.GestionAgenceLocation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ProjetJEE.GestionAgenceLocation.dao.CategoryRepository;
import com.ProjetJEE.GestionAgenceLocation.entities.Category;

@Service
public class ServiceCategory implements IserviceCategory{
	
	@Autowired
	CategoryRepository cr;
	
	@Override              //une méthode qui implémente une interface
	public void saveCategory(Category a) {
		cr.save(a);
	};
	
	@Override
	public Category getCategory(int id) {
		return cr.findById(id).get();
	};
	
	@Override
	public void deleteCategory(int id) {
		cr.deleteById(id);
	};
	
	@Override
	public List<Category> getAllCategory(){
		return cr.findAll();
	};
	
}
