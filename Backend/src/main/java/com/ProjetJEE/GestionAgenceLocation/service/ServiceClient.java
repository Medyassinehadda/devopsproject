package com.ProjetJEE.GestionAgenceLocation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ProjetJEE.GestionAgenceLocation.dao.ClientRepository;
import com.ProjetJEE.GestionAgenceLocation.entities.Client;

@Service
public class ServiceClient implements IserviceClient{

	@Autowired
	ClientRepository cr;
	
	@Override
	public void saveClient(Client a) {
		cr.save(a);
	};
	
	@Override
	public Client getClient(int id) {
		return cr.findById(id).get();
	};
	
	@Override
	public void deleteClient(int id) {
		cr.deleteById(id);
	};
	
	@Override
	public List<Client> getClientByCin(int cin){
		return cr.rechercheParCin(cin);
	};
	
	@Override
	public List<Client> getClientByEmail(String email){
		return cr.rechercheParEmail(email);
	};
	
	@Override
	public List<Client> getClientByNom(String nom){
		return cr.rechercheParNom(nom);
	};
	
	@Override
	public List<Client> getClientByPrenom(String prenom){
		return cr.rechercheParPrenom(prenom);
	};

	@Override
	public List<Client> getClientByAdresse(String adresse){
		return cr.rechercheParAdresse(adresse);
	};
	
	@Override
	public List<Client> getClientByNumPermis(int numpermis){
		return cr.rechercheParNumPermis(numpermis);
	};
	
	@Override
	public List<Client> getClientBytelephone(int telephone){
		return cr.rechercheParTelephone(telephone);
	};
	
	@Override
	public List<Client> getAllClients(){
		return cr.findAll();
	};
	
}
