package com.ProjetJEE.GestionAgenceLocation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ProjetJEE.GestionAgenceLocation.dao.ContratRepository;
import com.ProjetJEE.GestionAgenceLocation.entities.Contrat;

@Service
public class ServiceContrat implements IserviceContrat{
	
	@Autowired
	ContratRepository cr;
	
	@Override
	public void saveContrat(Contrat c) {
		cr.save(c);
	};
	
	@Override
	public Contrat getContrat(int id) {
		return cr.findById(id).get();
	};
	
	@Override
	public void deleteContrat(int id) {
		cr.deleteById(id);
	};
	
	@Override
	public List<Contrat> getContratByClientId(int clientid){
		return cr.rechercheParClientId(clientid);
	};
	
	@Override
	public List<Contrat> getContratByVoitureId(int voitureid){
		return cr.rechercheParVoitureId(voitureid);
	};
	
	@Override
	public List<Contrat> getContratByCautionnement(String cautionnement){
		return cr.rechercheParCautionnement(cautionnement);
	};
	
	@Override
	public List<Contrat> getContratByDateDebut(String datedebut){
		return cr.rechercheParDateDebut(datedebut);
	};
	
	@Override
	public List<Contrat> getContratByDateFin(String datefin){
		return cr.rechercheParDateFin(datefin);
	};
	
	@Override
	public List<Contrat> getContratByMontant(String montant){
		return cr.rechercheParMontant(montant);
	};
	
	@Override
	public List<Contrat> getContratByTypePayement(String typepayement){
		return cr.rechercheParTypePayement(typepayement);
	};
	
	@Override
	public List<Contrat> getContratByPeriodeLocation(String periodelocation){
		return cr.rechercheParPeriodeLocation(periodelocation);
	};

	@Override
	public List<Contrat> getAllContrats(){
		return cr.findAll();
	};
	
}
