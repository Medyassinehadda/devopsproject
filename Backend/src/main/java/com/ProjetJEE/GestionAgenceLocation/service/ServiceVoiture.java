package com.ProjetJEE.GestionAgenceLocation.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ProjetJEE.GestionAgenceLocation.dao.CategoryRepository;
import com.ProjetJEE.GestionAgenceLocation.dao.VoitureRepository;
import com.ProjetJEE.GestionAgenceLocation.entities.Voiture;

@Service
public class ServiceVoiture implements IserviceVoiture{

	@Autowired
	VoitureRepository vr;
	@Autowired
	CategoryRepository cr;
	
	@Override
	public void saveVoiture(Voiture a) {
		vr.save(a);
	};
	
	@Override
	public Voiture getVoiture(int id) {
		return vr.findById(id).get();
	};
	
	@Override
	public void deleteVoiture(int id) {
		vr.deleteById(id);
	};

	@Override
	public List<Voiture> getVoitureByMatricule(String matricule){
		return vr.rechercheParMatricule(matricule);
	};
	
	@Override
	public List<Voiture> getVoitureByCouleur(String couleur){
		return vr.rechercheParCouleur(couleur);
	};

	@Override
	public List<Voiture> getVoitureByMarque(String marque){
		return vr.rechercheParMarque(marque);
	};
	
	@Override
	public List<Voiture> getVoitureByModele(String modele){
		return vr.rechercheParModele(modele);
	};
	
	@Override
	public List<Voiture> getVoitureByPuissance(String puissance){
		return vr.rechercheParPuissance(puissance);
	};
	
	
	@Override
	public List<Voiture> getVoitureByCoutParJour(String coutparjour){
		return vr.rechercheParCoutParJour(coutparjour);
	};
	
	@SuppressWarnings("deprecation")
	@Override
	public List<Voiture> getVoitureByCategoryId(int catgeoryid){
		return cr.getById(catgeoryid).getListe();
	};

//	@Override
//	public List<Voiture> getVoitureByContratId(int contratid){
//		return vr.rechercheParContratId(contratid);
//	};
	
	@Override
	public List<Voiture> getAllVoitures(){
		return vr.findAll();
	};
	
	
	@Override
	public String saveImage(MultipartFile mf) throws IOException {
		String nameFile = mf.getOriginalFilename();
		String tab[] = nameFile.split("\\.");
		String fileModif = tab[0]+"_"+System.currentTimeMillis()+"."+tab[1];
		String chemin = System.getProperty("user.dir")+"/src/main/webapp/imagesdata/";
		Path p = Paths.get(chemin,fileModif);
		Files.write(p, mf.getBytes());
		
		return fileModif;
	}
	
	@Override
	public void saveVoitureWithPicture(Voiture v,MultipartFile mf) throws IOException {
		String photo;
		if(!mf.getOriginalFilename().equals("")) {
			photo = saveImage(mf);
			v.setPhoto(photo);
		}
		vr.save(v);
	}
	
	@Override
	public void deleteImage(Integer voitureId) throws IOException {
		Voiture voiture = vr.findById(voitureId).get();
		String chemin = System.getProperty("user.dir")+"/src/main/webapp/imagesdata/";
		Path p = Paths.get(chemin,voiture.getPhoto());
		Files.delete(p);
	}
	
	@Override
	public void deleteVoiture(Integer id) throws IOException {
		if(!vr.findById(id).get().getPhoto().equals("")) {
			deleteImage(id);
		}
		vr.deleteById(id);
	}
	
	@Override
	public byte[] getImage(int id) throws IOException {
		String nomImage=vr.findById(id).get().getPhoto();
		String chemin = System.getProperty("user.home")+"/imagesdata";
		Path p = Paths.get(chemin,nomImage);
		
		return Files.readAllBytes(p);
	}
	
}
