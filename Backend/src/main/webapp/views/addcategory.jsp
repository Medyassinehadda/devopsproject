<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
	<title>Ajouter Categorie</title>
</head>
<body>
	
	<%@include file="navbar.html" %>
	
	<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
			<c:out value="${message}"></c:out>
			${category=null}
		</div>
	
	</c:if>
	
	<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter Categorie</h2></div>
			<div class="card-body">
				<form action="/apicategory/savecategory" method=post >
					<div class="mb-3 mt-3">
						<label for="gamme" class="form-label">Gamme:</label>
						<input type=text class="form-control" id="gamme" name="gamme" value="${categorie.gamme}">
					</div>
					<input type="hidden" name="id" value="${categorie.id}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
	
</body>
</html>