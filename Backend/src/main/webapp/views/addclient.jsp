<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
	<title>Ajouter Client</title>
</head>
<body>
	
	<%@include file="navbar.html" %>
	
	<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
			<c:out value="${message}"></c:out>
			${client=null}
		</div>
	
	</c:if>
	
	<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter Client</h2></div>
			<div class="card-body">
				<form action="/apiclient/saveclient" method=post>
					<div class="mb-3 mt-3">
						<label for="cin" class="form-label">CIN:</label>
						<input type=number class="form-control" id="cin" name="cin" value="${client.cin}">
					</div>
					<div class="mb-3 mt-3">
						<label for="nom" class="form-label">Nom:</label>
						<input type=text class="form-control" id="nom" name="nom" value="${client.nom}">
					</div>
					<div class="mb-3 mt-3">
						<label for="prenom" class="form-label">Pr�nom:</label>
						<input type=text class="form-control" id="prenom" name="prenom" value="${client.prenom}">
					</div>
					<div class="mb-3 mt-3">
						<label for="date_naissance" class="form-label">Date de naissance:</label>
						<input type=text class="form-control" id="date_naissance" name="date_naissance" value="${client.date_naissance}">
					</div>
					<div class="mb-3 mt-3">
						<label for="telephone" class="form-label">Telephone:</label>
						<input type=number class="form-control" id="telephone" name="telephone" value="${client.telephone}">
					</div>
					<div class="mb-3 mt-3">
						<label for="num_permis" class="form-label">Num�ro permis:</label>
						<input type=number class="form-control" id="num_permis" name="num_permis" value="${client.num_permis}">
					</div>
					<div class="mb-3 mt-3">
						<label for="email" class="form-label">Email:</label>
						<input type=text class="form-control" id="email" name="email" value="${client.email}">
					</div>
					<div class="mb-3 mt-3">
						<label for="password" class="form-label">Password:</label>
						<input type=password class="form-control" id="password" name="password" value="${client.password}">
					</div>
					
					<input type="hidden" name="id" value="${client.id}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
	
</body>
</html>