<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
	<title>Ajouter Contrat</title>
</head>
<body>
	
	<%@include file="navbar.html" %>
	
	<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
			<c:out value="${message}"></c:out>
			${contrat=null}
		</div>
	
	</c:if>
	
	<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter Contrat</h2></div>
			<div class="card-body">
				<form action="/apicontrat/savecontrat" method=post enctype="multipart/form-data">
					<div class="mb-3 mt-3">
						<label for="periode_location" class="form-label">Periode location:</label>
						<input type=text class="form-control" id="periode_location" name="periode_location" value="${contrat.periode_location}">
					</div>
					<div class="mb-3 mt-3">
						<label for="date_debut" class="form-label">Date debut:</label>
						<input type=text class="form-control" id="date_debut" name="date_debut" value="${contrat.date_debut}">
					</div>
					<div class="mb-3 mt-3">
						<label for="date_fin" class="form-label">Date fin:</label>
						<input type=text class="form-control" id="date_fin" name="date_fin" value="${contrat.date_fin}">
					</div>
					<div class="mb-3 mt-3">
						<label for="type_payement" class="form-label">Type payement:</label>
						<input type=text class="form-control" id="type_payement" name="type_payement" value="${contrat.type_payement}">
					</div>
					<div class="mb-3 mt-3">
						<label for="montant" class="form-label">Montant:</label>
						<input type=text class="form-control" id="montant" name="montant" value="${contrat.montant}">
					</div>
					<div class="mb-3 mt-3">
						<label for="cautionnement" class="form-label">Cautionnement:</label>
						<input type=text class="form-control" id="cautionnement" name="cautionnement" value="${contrat.cautionnement}">
					</div>
					<div class="mb-3">
						<label for="client" class="form-label">Client:</label>
						<select name="client" class="form-control" >
							<option selected hidden value="client">Choose here</option>
							<c:forEach items="${Clients}" var="cc">
								<option value="${cc.id}" <c:if test="${contrat.client.id==cc.id}">selected="true"</c:if>>${cc.cin} </option>
							</c:forEach>
						</select>
					</div>
					<div class="mb-3">
						<label for="client" class="form-label">Voiture:</label>
						<select name="voiture" class="form-control" >
							<option selected hidden value="voiture">Choose here</option>
							<c:forEach items="${Voitures}" var="cc">
								<option value="${cc.id}" <c:if test="${contrat.voiture.id==cc.id}">selected="true"</c:if>>${cc.matricule} </option>
							</c:forEach>
						</select>
					</div>

					<input type="hidden" name="id" value="${contrat.id}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
	
</body>
</html>