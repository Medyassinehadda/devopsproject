<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
	<title>Ajouter Voiture</title>
</head>
<body>
	
	<%@include file="navbar.html" %>
	
	<c:if test="${message!=null}">
		<div class="alert alert-success" role="alert">
			<c:out value="${message}"></c:out>
			${voiture=null}
		</div>
	
	</c:if>
	
	<div class=container>
		<div class="card m-5 p-2">
			<div class="card-header"><h2>Ajouter Voiture</h2></div>
			<div class="card-body">
				<form action="/apivoiture/savevoiture" method=post enctype="multipart/form-data">
					<div class="mb-3 mt-3">
						<label for="matricule" class="form-label">Matricule:</label>
						<input type=text class="form-control" id="matricule" name="matricule" value="${voiture.matricule}">
					</div>
					<div class="mb-3 mt-3">
						<label for="marque" class="form-label">Marque:</label>
						<input type=text class="form-control" id="marque" name="marque" value="${voiture.marque}">
					</div>
					<div class="mb-3 mt-3">
						<label for="modele" class="form-label">Modele:</label>
						<input type=text class="form-control" id="modele" name="modele" value="${voiture.modele}">
					</div>
					<div class="mb-3 mt-3">
						<label for="couleur" class="form-label">Couleur:</label>
						<input type=text class="form-control" id="couleur" name="couleur" value="${voiture.couleur}">
					</div>
					<div class="mb-3 mt-3">
						<label for="puissance" class="form-label">Puissance:</label>
						<input type=text class="form-control" id="puissance" name="puissance" value="${voiture.puissance}">
					</div>
					<div class="mb-3 mt-3">
						<label for="cout_par_jour" class="form-label">Cout par jour:</label>
						<input type=text class="form-control" id="cout_par_jour" name="cout_par_jour" value="${voiture.cout_par_jour}">
					</div>
					<div class="mb-3">
						<label for="category" class="form-label">Categorie:</label>
						<%-- <select class="form-control" name="categorie">
							<option selected hidden value="categorie">Choose here</option>
							<c:forEach items="${categories}" var="cc">
								<option value="${cc.id}">${cc.gamme}</option>
							</c:forEach>
						</select> --%>
						<select name="category" class="form-control" >
							<option selected hidden value="category">Choose here</option>
							<c:forEach items="${categories}" var="cc">
								<option value="${cc.id}" <c:if test="${voiture.category.id==cc.id}">selected="true"</c:if>>${cc.gamme} </option>
							</c:forEach>
						</select>
					</div>
					<div class="mb-3">
						<input type="hidden" name="photo" value="${voiture.photo}" />
						<label for="photo" class="form-label">Photo:</label>
						<input type="file" name="file" accept="image/png, image/jpeg" class="form-control">
					</div>
					<input type="hidden" name="id" value="${voiture.id}">
					<button type="submit" class="btn btn-primary" >Save</button>
				</form>
			</div> 
		</div>
	</div>
	
</body>
</html>