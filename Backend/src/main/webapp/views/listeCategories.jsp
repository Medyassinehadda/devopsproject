<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
	<title>liste categories</title>
</head>
<body>

	<%@include file="navbar.html" %>
	
	<div class="container mt-3">
	  <h2>Table Categories</h2>
	  <a href="/apicategory/addcategory" class="btn btn-primary" styles="align">Add Categorie</a>

	  <!-- <p>Vous pouvez ajouter des categories on utilisons le bouton Add Categorie ci dessus</p> -->            
	  <table class="table table-striped">
	    <thead>
	      <tr>
	      	<th>Id</th>
			<th>Gamme</th>
			<th>Action</th>
	      </tr>
	    </thead>
	    <tbody>
			<c:forEach items="${Categories}" var="v">
				<tr>
					<td>${v.id}</td>
					<td>${v.gamme}</td>
					<td>
						<a href="deletecategory/${v.id}" class="btn btn-danger">
							<i class='fas fa-trash-alt'></i>
						</a>
						<a href="modifiercategory/${v.id}" class="btn btn-warning">
							<span class="fa fa-edit"></span>
						</a>
					</td>
				</tr>
			</c:forEach>
	    </tbody>
	  </table>
	</div>
	
</body>
</html>