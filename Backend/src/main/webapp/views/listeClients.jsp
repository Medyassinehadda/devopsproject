<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
	<title>liste clients</title>
</head>
<body>

	<%@include file="navbar.html" %>
	
	<div class="container mt-3">
	  <h2>Table clients</h2>
	  
      <br>
      
	  <a href="/apiclient/addclient" class="btn btn-primary" styles="align">Add Client</a>
	  
	  <form class="d-flex" method="get" action="/apiclient/clientbyprenom">
		<div class=container>
			<div class="row mt-4 pl-2">
				<div class="col-md-2">
       				<input class="form-control me-2" type="text" placeholder="Search By Prenom" name="pre" id="pre">
       			</div>
       			<div class="col-md-8">
       				<input type="submit" class="btn btn-primary" value="Search">
       			</div>
       		</div>
       	</div>
      </form>
	  
	  <table class="table table-striped">
	    <thead>
	      <tr>
			<th>Id</th>
			<th>CIN</th>
			<th>Nom</th>
			<th>Prenom</th>
			<th>Date de naissance</th>
			<th>Telephone</th>
			<th>Num�ro permis</th>
			<th>Email</th>
			<th>Action</th>
	      </tr>
	    </thead>
	    <tbody>
		<c:forEach items="${Clients}" var="c">
		
			<tr>
				<td>${c.id}</td>
				<td>${c.cin}</td>
				<td>${c.nom}</td>
				<td>${c.prenom}</td>
				<td>${c.date_naissance}</td>
				<td>${c.telephone}</td>
				<td>${c.num_permis}</td>
				<td>${c.email}</td>
				<td>
					<a href="deleteclient/${c.id}" class="btn btn-danger">
						<i class='fas fa-trash-alt'></i>
					</a>
					<a href="modifierclient/${c.id}" class="btn btn-warning">
						<span class="fa fa-edit"></span>
					</a>
				</td>
			</tr>
		</c:forEach>
	    </tbody>
	  </table>
	</div>

</body>
</html>