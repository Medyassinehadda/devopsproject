<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
	<title>liste contrats</title>
</head>
<body>

	<%@include file="navbar.html" %>
	
	<div class="container mt-3">
	  <h2>Table contrats</h2>
	  
      <br>
      
	  <a href="/apicontrat/addcontrat" class="btn btn-primary" styles="align">Add Contrat</a>
	  
	  <form class="d-flex" method="get" action="/apicontrat/clientbycin">
		<div class=container>
			<div class="row mt-4 pl-2">
				<div class="col-md-2">
       				<input class="form-control me-2" type="text" placeholder="Search By CIN" name="cin" id="cin">
       			</div>
       			<div class="col-md-8">
       				<input type="submit" class="btn btn-primary" value="Search">
       			</div>
       		</div>
       	</div>
      </form>
	  
	  <table class="table table-striped">
	    <thead>
	      <tr>
			<th>Id</th>
			<th>Periode location</th>
			<th>Date debut</th>
			<th>Date fin</th>
			<th>Type payement</th>
			<th>Montant</th>
			<th>Cautionnement</th>
			<th>Client</th>
			<th>Voiture</th>
			<th>Action</th>
	      </tr>
	    </thead>
	    <tbody>
		<c:forEach items="${Contrats}" var="c">
		
			<tr>
				<td>${c.id}</td>
				<td>${c.periode_location}</td>
				<td>${c.date_debut}</td>
				<td>${c.date_fin}</td>
				<td>${c.type_payement}</td>
				<td>${c.montant}</td>
				<td>${c.cautionnement}</td>
				<td>${c.client.id}</td>
				<td>${c.voiture.id}</td>
				<td>
					<a href="deletecontrat/${c.id}" class="btn btn-danger">
						<i class='fas fa-trash-alt'></i>
					</a>
					<a href="modifiercontrat/${c.id}" class="btn btn-warning">
						<span class="fa fa-edit"></span>
					</a>
				</td>
			</tr>
		</c:forEach>
	    </tbody>
	  </table>
	</div>

</body>
</html>