<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
	<title>liste voitures</title>
</head>
<body>

	<%@include file="navbar.html" %>
	
	<div class="container mt-3">
	  <h2>Table voitures</h2>
	  
      <br>
      
	  <a href="/apivoiture/addvoiture" class="btn btn-primary" styles="align">Add Voiture</a>
	  
	  <form action="/apivoiture/voiturebycategory" method=get>
		<div class=container>
			<div class="row mt-4 pl-2">
				<div class="col-md-1">
					<label for="mc" class="form-label">Categories:</label>
				</div>
				<div class="col-md-3">
					<select class="form-control" name="categorie" onchange="submit()">
						<option selected hidden>${categorie}</option>
						<option value="0"> Toutes les categories </option>
						<c:forEach items="${categories}" var="cc">
							<option value="${cc.id}">${cc.gamme}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div> 
	  </form>
	  <form class="d-flex" method="get" action="/apivoiture/voiturebymarque">
		<div class=container>
			<div class="row mt-4 pl-2">
				<div class="col-md-2">
       				<input class="form-control me-2" type="text" placeholder="Search By Marque" name="mc" id="mc">
       			</div>
       			<div class="col-md-8">
       				<input type="submit" class="btn btn-primary" value="Search">
       			</div>
       		</div>
       	</div>
      </form>
	  
	  <table class="table table-striped">
	    <thead>
	      <tr>
	      	<th>Photo</th>
			<th>Id</th>
			<th>Matricule</th>
			<th>Marque</th>
			<th>Modele</th>
			<th>Couleur</th>
			<th>Puissance</th>
			<th>Categorie</th>
			<th>Cout par jour</th>
			<th>Action</th>
	      </tr>
	    </thead>
	    <tbody>
		<c:forEach items="${Voitures}" var="v">
		
			<tr>
				<td>
					<c:choose>
						<c:when test="${v.photo==''}">
							<img src="/imagesdata/inconnu.png" width=50 height=50>
						</c:when>
						<c:when test="${v.photo!=''}">
							<img src="/imagesdata/${v.photo}" width=50 height=50>
						</c:when>
					</c:choose>
				</td>
				<td>${v.id}</td>
				<td>${v.matricule}</td>
				<td>${v.marque}</td>
				<td>${v.modele}</td>
				<td>${v.couleur}</td>
				<td>${v.puissance}</td>
				<td>${v.category.gamme}</td>
				<td>${v.cout_par_jour}</td>
				<td>
					<a href="deletevoiture/${v.id}" class="btn btn-danger">
						<i class='fas fa-trash-alt'></i>
					</a>
					<a href="modifiervoiture/${v.id}" class="btn btn-warning">
						<span class="fa fa-edit"></span>
					</a>
				</td>
			</tr>
		</c:forEach>
	    </tbody>
	  </table>
	</div>

</body>
</html>