import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//add
import { AddcategoryComponent } from './components/categorys/addcategory/addcategory.component';
import { AddclientComponent } from './components/clients/addclient/addclient.component';
import { AddcontratComponent } from './components/contrats/addcontrat/addcontrat.component';
import { AddvoitureComponent } from './components/voitures/addvoiture/addvoiture.component';
//lists
import { ListecategoryComponent } from './components/categorys/listecategory/listecategory.component';
import { ListeclientComponent } from './components/clients/listeclient/listeclient.component';
import { ListecontratComponent } from './components/contrats/listecontrat/listecontrat.component';
import { ListevoitureComponent } from './components/voitures/listevoiture/listevoiture.component';
//update
import { UpdatecategoryComponent } from './components/categorys/updatecategory/updatecategory.component';
import { UpdateclientComponent } from './components/clients/updateclient/updateclient.component';
import { UpdatecontratComponent } from './components/contrats/updatecontrat/updatecontrat.component';
import { UpdatevoitureComponent } from './components/voitures/updatevoiture/updatevoiture.component';
//accueil
import { AccueilComponent } from './components/accueil/accueil/accueil.component';

const routes: Routes = [
  {path:"categorylist",component:ListecategoryComponent},
  {path:"category/add",component:AddcategoryComponent},
  {path:"category/:id",component:UpdatecategoryComponent},
  {path:"clientlist",component:ListeclientComponent},
  {path:"client/add",component:AddclientComponent},
  {path:"client/:id",component:UpdateclientComponent},
  {path:"contratlist",component:ListecontratComponent},
  {path:"contrat/add",component:AddcontratComponent},
  {path:"contrat/:id",component:UpdatecontratComponent},
  {path:"voiturelist",component:ListevoitureComponent},
  {path:"voiture/add",component:AddvoitureComponent},
  {path:"voiture/:id",component:UpdatevoitureComponent},
  {path:"accueil",component:AccueilComponent},
  {path:"",redirectTo:"/accueil",pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
