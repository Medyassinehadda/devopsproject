import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//lists
import { ListevoitureComponent } from './components/voitures/listevoiture/listevoiture.component';
import { ListeclientComponent } from './components/clients/listeclient/listeclient.component';
import { ListecontratComponent } from './components/contrats/listecontrat/listecontrat.component';
import { ListecategoryComponent } from './components/categorys/listecategory/listecategory.component';
//add
import { AddvoitureComponent } from './components/voitures/addvoiture/addvoiture.component';
import { AddclientComponent } from './components/clients/addclient/addclient.component';
import { AddcontratComponent } from './components/contrats/addcontrat/addcontrat.component';
import { AddcategoryComponent } from './components/categorys/addcategory/addcategory.component';
//update
import { UpdatevoitureComponent } from './components/voitures/updatevoiture/updatevoiture.component';
import { UpdatecategoryComponent } from './components/categorys/updatecategory/updatecategory.component';
import { UpdatecontratComponent } from './components/contrats/updatecontrat/updatecontrat.component';
import { UpdateclientComponent } from './components/clients/updateclient/updateclient.component';
import { AccueilComponent } from './components/accueil/accueil/accueil.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AddvoitureComponent,
    ListevoitureComponent,
    ListeclientComponent,
    AddclientComponent,
    AddcontratComponent,
    ListecontratComponent,
    AddcategoryComponent,
    ListecategoryComponent,
    UpdatevoitureComponent,
    UpdatecategoryComponent,
    UpdatecontratComponent,
    UpdateclientComponent,
    AccueilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
