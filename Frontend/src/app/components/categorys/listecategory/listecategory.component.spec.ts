import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListecategoryComponent } from './listecategory.component';

describe('ListecategoryComponent', () => {
  let component: ListecategoryComponent;
  let fixture: ComponentFixture<ListecategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListecategoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListecategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
