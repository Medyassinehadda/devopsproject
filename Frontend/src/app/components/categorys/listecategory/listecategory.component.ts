import { Component,OnInit } from '@angular/core';
import { Category } from 'src/app/model/category';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-listecategory',
  templateUrl: './listecategory.component.html',
  styleUrls: ['./listecategory.component.css']
})

export class ListecategoryComponent implements OnInit {

  constructor(private service:CategoryService) {}
  
  categorys:Category[]
  
  ngOnInit() {
    this.service.getAllCategory().subscribe((data: Category[])=>this.categorys=data)
  }
}
