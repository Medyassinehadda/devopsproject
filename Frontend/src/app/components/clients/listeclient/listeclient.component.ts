import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/model/client';
import { ClientService } from 'src/app/service/client.service';

@Component({
  selector: 'app-listeclient',
  templateUrl: './listeclient.component.html',
  styleUrls: ['./listeclient.component.css']
})

export class ListeclientComponent implements OnInit{

  constructor(private service:ClientService) {}
  
  clients:Client[]
  
  ngOnInit() {
    this.service.getAllClients().subscribe((data: Client[])=>this.clients=data)
  }
}
