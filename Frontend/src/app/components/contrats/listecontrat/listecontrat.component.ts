import { Component, OnInit } from '@angular/core';
import { Contrat } from 'src/app/model/contrat';
import { ContratService } from 'src/app/service/contrat.service';

@Component({
  selector: 'app-listecontrat',
  templateUrl: './listecontrat.component.html',
  styleUrls: ['./listecontrat.component.css']
})
export class ListecontratComponent implements OnInit{

  constructor(private service:ContratService) {}
  
  contrats:Contrat[]
  
  ngOnInit() {
    this.service.getAllContrats().subscribe((data: Contrat[])=>this.contrats=data)
  }
}
