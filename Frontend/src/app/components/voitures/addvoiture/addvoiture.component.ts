import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from 'src/app/model/category';
import { Voiture } from 'src/app/model/voiture';
import { CategoryService } from 'src/app/service/category.service';
import { VoitureService } from 'src/app/service/voiture.service';

@Component({
  selector: 'app-addvoiture',
  templateUrl: './addvoiture.component.html',
  styleUrls: ['./addvoiture.component.css']
})
export class AddvoitureComponent implements OnInit{

  constructor(
    private serviceCat:CategoryService,
    private serviceVoi:VoitureService,
    private router:Router) { }
  
    categorys:Category[]
  
    ngOnInit() {
      this.serviceCat.getAllCategory().subscribe((data: Category[])=>this.categorys=data)
    }
    
    save(fd:NgForm){
      let formd:FormData=new FormData();
      formd.append("file",this.file)
      let voiture:Voiture=fd.value
    

      console.log(JSON.stringify(voiture.category))
      
      formd.append("voiture",JSON.stringify(voiture))
        this.serviceVoi.saveVoiture(formd).subscribe(
        ()=>this.router.navigate(['/voiturelist'])
        )
      }
      file:File
      selectimage(event)
      {
      this.file=event.target.files[0];

      }
}
