import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListevoitureComponent } from './listevoiture.component';

describe('ListevoitureComponent', () => {
  let component: ListevoitureComponent;
  let fixture: ComponentFixture<ListevoitureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListevoitureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListevoitureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
