import { Component, OnInit } from '@angular/core';
import { Voiture } from 'src/app/model/voiture';
import { VoitureService } from 'src/app/service/voiture.service';

@Component({
  selector: 'app-listevoiture',
  templateUrl: './listevoiture.component.html',
  styleUrls: ['./listevoiture.component.css']
})
export class ListevoitureComponent implements OnInit {

  constructor(private service:VoitureService) {}
  
  voitures:Voiture[]
  
  ngOnInit() {
    this.service.getAllVoitures().subscribe((data: Voiture[])=>this.voitures=data)
  }
}
