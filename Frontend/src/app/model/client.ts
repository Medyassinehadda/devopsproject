export interface Client {
    id:number;
    email: string;
    password: string;
    cin: number;
    nom: string;
    prenom: string;
    date_naissance: string;
    adresse: string;
    telephone: number;	
    num_permis: number;
}
