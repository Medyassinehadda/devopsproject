import { Client } from "./client";
import { Voiture } from "./voiture";

export interface Contrat {
    id: number;
    periode_location: string;
    date_debut: string;
    date_fin: string;
    type_payement: string;
    montant: string;
    cautionnement: string;
    client: Client;
    voiture: Voiture;
}
