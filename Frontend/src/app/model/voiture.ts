import { Category } from "./category";

export interface Voiture {
    id: number;
    photo: string;
    matricule: string;
    marque: string;
    modele: string;
    couleur: string;
    puissance: string;
    category: Category;
    cout_par_jour: string
}
