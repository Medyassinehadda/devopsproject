import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from '../model/category';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  host= "http://localhost:8080/restapicategory"
  
  constructor(private client:HttpClient) {}

  public categorys:Category[]
  
  //******************************************************** GET ALL CATEGORY **************************************************/
  public getAllCategory():Observable<Category[]>
  {return this.client.get<Category[]>(this.host+"/all");}
  
  //******************************************************** ADD CATEGORY **************************************************/
  public addCategory(category:Category):Observable<void>
  {return this.client.post<void>(this.host+"/save",category);}
  
  //******************************************************** UPDATE CATEGORY **************************************************/
  public updateCategory(id: number):Observable<number>
  {return this.client.get<number>(this.host+"/update/"+id)}

  //******************************************************** DELETE CATEGORY **************************************************/
  public deleteCategory(id: number):Observable<void>
  {return this.client.delete<void>(this.host+"/delete/"+id)}

}
