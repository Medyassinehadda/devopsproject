import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../model/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  host= "http://localhost:8080/restapiclient"
  
  constructor(private client:HttpClient) {}

  public clients:Client[]

  //******************************************************** GET ALL CLIENT **************************************************/
  public getAllClients():Observable<Client[]>
  {return this.client.get<Client[]>(this.host+"/all");}
  
  //******************************************************** ADD CLIENT **************************************************/
  public addClient(client:Client):Observable<void>
  {return this.client.post<void>(this.host+"/saveclient",client);}
  
  //******************************************************** UPDATE CLIENT **************************************************/
  public updateClient(id: number):Observable<void>
  {return this.client.get<void>(this.host+"/modifierclient/"+id)}

  //******************************************************** DELETE CLIENT **************************************************/
  public deleteClient(id: number):Observable<void>
  {return this.client.delete<void>(this.host+"/delete/"+id)}
  
  /************************************************************************************************************************************************* */

  //******************************************************** GET CLIENT BY TEL **************************************************/
  public getClientByTel(num: number):Observable<Client[]>
  {return this.client.get<Client[]>(this.host+"/clientbytel"+num);}

  //******************************************************** GET CLIENT BY PRENOM **************************************************/
  public getClientByPrenom(prenom: string):Observable<Client[]>
  {return this.client.get<Client[]>(this.host+"/clientbyprenom"+prenom);}

  //******************************************************** GET CLIENT BY NUM PERMIS **************************************************/
  public getClientByNumPermis(numper:number):Observable<Client[]>
  {return this.client.get<Client[]>(this.host+"/clientbynumpermis"+numper);}

  //******************************************************** GET CLIENT BY NOM **************************************************/
  public getClientByNom(nom:string):Observable<Client[]>
  {return this.client.get<Client[]>(this.host+"/clientbynom"+nom);}

  //******************************************************** GET CLIENT BY EMAIL **************************************************/
  public getClientByEmail(email:string):Observable<Client[]>
  {return this.client.get<Client[]>(this.host+"/clientbyemail"+email);}

  //******************************************************** GET CLIENT BY CIN **************************************************/
  public getClientByCin(cin:number):Observable<Client[]>
  {return this.client.get<Client[]>(this.host+"/clientbycin"+cin);}

  //******************************************************** GET CLIENT BY ADRESSE **************************************************/
  public getClientByAdresse(adresse:string):Observable<Client[]>
  {return this.client.get<Client[]>(this.host+"/clientbyadresse"+adresse);}

}
