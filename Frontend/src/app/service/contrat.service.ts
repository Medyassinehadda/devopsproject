import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contrat } from '../model/contrat';

@Injectable({
  providedIn: 'root'
})
export class ContratService {

  host= "http://localhost:8080/restapicontrat"
  
  constructor(private client:HttpClient) {}

  public contrats:Contrat[]

  //******************************************************** GET ALL CONTRAT **************************************************/
  public getAllContrats():Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/all");}
  
  //******************************************************** ADD CONTRAT **************************************************/
  public addContrat(contrat:Contrat):Observable<void>
  {return this.client.post<void>(this.host+"/savecontrat",contrat);}
  
  //******************************************************** UPDATE CONTRAT **************************************************/
  public updateContrat(id: number):Observable<void>
  {return this.client.get<void>(this.host+"/modifiercontrat/"+id)}

  //******************************************************** DELETE CONTRAT **************************************************/
  public deleteContrat(id: number):Observable<void>
  {return this.client.delete<void>(this.host+"/delete/"+id)}
  
  /************************************************************************************************************************************************* */

  //******************************************************** GET CONTRAT BY periodeLocation **************************************************/
  public getClientByPeriodelocation(periodeloc: string):Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/getcontratbyperiodelocation"+periodeloc);}

  //******************************************************** GET CONTRAT BY TypePayement **************************************************/
  public getClientByTypepayement(typepayement: string):Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/contratbytypepayement"+typepayement);}

  //******************************************************** GET CONTRAT BY Montant **************************************************/
  public getClientByMontant(montant: string):Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/contratbymontant"+montant);}

  //******************************************************** GET CONTRAT BY DateFin **************************************************/
  public getClientByDatefin(datefin: string):Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/contratbydatefin"+datefin);}

  //******************************************************** GET CONTRAT BY DateDebut **************************************************/
  public getClientByDatedebut(datedebut: string):Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/contratbydatedebut"+datedebut);}

  //******************************************************** GET CONTRAT BY Cautionnement **************************************************/
  public getClientByCautionnement(cautionnement: string):Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/contratbycautionnement"+cautionnement);}

  //******************************************************** GET CONTRAT BY VoitureId **************************************************/
  public getClientByVoitureId(id:number):Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/contratbyvoiture/"+id);}

  //******************************************************** GET CONTRAT BY CLIENT **************************************************/
  public getClientByCLientId(id:number):Observable<Contrat[]>
  {return this.client.get<Contrat[]>(this.host+"/contratbyclient/"+id);}

}
