import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Voiture } from '../model/voiture';


@Injectable({
  providedIn: 'root'
})
export class VoitureService {

  host= "http://localhost:8080/restapivoiture"
  
  constructor(private client:HttpClient) {}

  public voitures:Voiture[]

  //******************************************************** GET ALL CONTRAT **************************************************/
  public getAllVoitures():Observable<Voiture[]>
  {return this.client.get<Voiture[]>(this.host+"/all");}
  
  //******************************************************** ADD CONTRAT **************************************************/
  public saveVoiture(fd:FormData):Observable<void>
  {return this.client.post<void>(this.host+"/savevoiture",fd);}
  
  //******************************************************** UPDATE CONTRAT **************************************************/
  public updateVoiture(fd: FormData):Observable<void>
  {return this.client.put<void>(this.host+"/update/", fd)}

  //******************************************************** DELETE CONTRAT **************************************************/
  public deleteVoiture(id: number):Observable<void>
  {return this.client.delete<void>(this.host+"/delete/"+id)}
  
  //******************************************************** GET VOITURE BY CATEGORY **************************************************/
  public getVoitureByCategory(id: number):Observable<Voiture[]>
  {return this.client.get<Voiture[]>(this.host+"/voiturebycategory/"+id);}

}
